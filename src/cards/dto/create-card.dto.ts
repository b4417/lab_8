import { IsString, IsUUID } from 'class-validator';

export class CreateCardDto {
  @IsString()
  name: string;

  @IsUUID()
  columnId: string;
}
